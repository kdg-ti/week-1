import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: deketelw
 * Date: 1-10-13
 * Time: 9:15
 * To change this template use File | Settings | File Templates.
 */
public class Middelste {
    public static void main(String[] args) {
        int eerste, tweede, derde;
        int hulp;

        System.out.print("Geef drie getallen:");
        Scanner scanner = new Scanner(System.in);
        eerste = scanner.nextInt();
        tweede = scanner.nextInt();
        derde = scanner.nextInt();

        if (eerste > tweede) {
            // wissel waarden om
            hulp = eerste;
            eerste = tweede;
            tweede = hulp;
        }

        if (tweede > derde) {
            // wissel waarden om
            hulp = tweede;
            tweede = derde;
            derde = hulp;
        }

        if (eerste > tweede) {
            // wissel waarden om
            hulp = eerste;
            eerste = tweede;
            tweede = hulp;
        }

        System.out.println(eerste + " " + tweede + " " + derde);
    }

}
