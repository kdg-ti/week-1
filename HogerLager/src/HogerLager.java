import java.util.Random;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: deketelw
 * Date: 19-9-13
 * Time: 12:26
 * To change this template use File | Settings | File Templates.
 */
public class HogerLager {
    public static void main(String[] args) {
        Random random = new Random();
        int teZoekenGetal = 1 + random.nextInt(100);
        int gok;
        int teller = 0;
        Scanner keyboard = new Scanner(System.in);

        System.out.print("Geef een getal:");
        while (true) {
            gok = keyboard.nextInt();
            teller = teller + 1;
            if (gok == teZoekenGetal) {
                System.out.println("Proficiat! U hebt het geraden in "
                        + teller + " pogingen");
                return;
            }

            if (gok < teZoekenGetal) {
                System.out.print("Te klein. Probeer opnieuw:");
            }

            if (gok > teZoekenGetal) {
                System.out.print("Te groot. Probeer opnieuw:");
            }
        }
    }
}
