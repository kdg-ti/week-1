# Hoger Lager

We **implementeren** het volgende algoritme:

    1. Kies getal en schrijf weg, noem dit teZoekenGetal
    2. Toon "Geef een getal" op output (scherm, bord, …).
    3. Lees input van keyboard en schrijf weg, noem dit gok.
    4. Is gok gelijk aan teZoekenGetal?
        – Ja: Schrijf "proficiat, u hebt het geraden" op het scherm en stop.
    5. Is gok kleiner dan teZoekenGetal?
        – Ja: Toon "Te klein" op output en keer terug naar stap 2
    6. Is gok groter dan teZoekenGetal?
        – Ja: Toon "Te groot" op output en keer terug naar stap 2

In deze opgave hebben te maken met:

* input en output (**IO**)
* Drie **testen**: we vergelijken de variabelen teZoekenGetal en gok
* Een **lus**
* **Tussenresultaten** die we even wegschrijven noemen we variabelen

#### We werken dit nu stap voor stap uit:
```java
    int teZoekenGetal = 56;
```
    2. Toon "Geef een getal" op output (scherm, bord, …).
    3. Lees input van keyboard en schrijf weg, noem dit gok.
    4. Is gok gelijk aan teZoekenGetal?
        – Ja: Schrijf "proficiat, u hebt het geraden" op het scherm en stop.
    5. Is gok kleiner dan teZoekenGetal?
        – Ja: Toon "Te klein" op output en keer terug naar stap 2
    6. Is gok groter dan teZoekenGetal?
        – Ja: Toon "Te groot" op output en keer terug naar stap 2

```java
    int teZoekenGetal = 56;
    System.out.print("Geef een getal: ");
```
    3. Lees input van keyboard en schrijf weg, noem dit gok.
    4. Is gok gelijk aan teZoekenGetal?
        – Ja: Schrijf "proficiat, u hebt het geraden" op het scherm en stop.
    5. Is gok kleiner dan teZoekenGetal?
        – Ja: Toon "Te klein" op output en keer terug naar stap 2
    6. Is gok groter dan teZoekenGetal?
        – Ja: Toon "Te groot" op output en keer terug naar stap 2
        
