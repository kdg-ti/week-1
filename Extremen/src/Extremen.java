import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: deketelw
 * Date: 1-10-13
 * Time: 9:06
 * To change this template use File | Settings | File Templates.
 */
public class Extremen {
    public static void main(String[] args) {
        int getal;
        int max = Integer.MIN_VALUE, min = Integer.MAX_VALUE;

        System.out.print("Geef een getal (stop met -1):");
        Scanner scanner = new Scanner(System.in);
        getal = scanner.nextInt();
        while (getal != -1) {
            if (getal > max)
                max = getal;
            if (getal < min)
                min = getal;
            System.out.print("Geef een getal (stop met -1):");
            getal = scanner.nextInt();
        }

        System.out.println("Het kleinste getal is: " + min);
        System.out.println("Het grootste getal is: " + max);
        System.out.println("Einde programma!");
    }
}
